import { Command } from './models/command';
import { FileStreamer } from './file-streamer';
import * as fs from 'fs';
export class CommandCollection {
    private commands: Command[] = new Array();
    private fileStreamer: FileStreamer = new FileStreamer();
    constructor() {
        this.reloadCommands();
    }

    find(query: string) {
        return this.commands.filter(x => x.command == query)[0];
    }

    findRandom(commandType: string = '') {
        let commands: Command[] = new Array();
        if(commandType == '') {
            commands = this.commands.filter(x => x.commandType == 'local' || x.commandType == 'web');            
        }
        else {
            commands = this.commands.filter(x => x.commandType == commandType);                        
        }
        let randomCommand = commands[Math.floor(Math.random() * commands.length)];
        if (randomCommand.command == 'supersuit') this.findRandom();
        else return randomCommand;
    }

    addCommand(command: Command) {
        this.commands.push(command);
    }

    getCommands() {
        let commands: string = '';
        for(var command of this.commands) {
            commands += `.${command.command}\n`;
        }
        return commands.slice(0, commands.length - 1);
    }

    reloadCommands() {
        this.fileStreamer.read('./media/commands.json')
        .then((data: Command[]) => {
            this.commands = data;
        })
        .catch((err: any) => {
            console.log(`Error reading File: ${err}`);
        });
    }

    updateCommands() {
        this.fileStreamer.write('./media/commands.json', this.commands)
        .catch((err: any) => {
            console.log(`Error writing File: ${err}`);         
        });
    }

    commandLevel(category: string) {
        let commandMemes = this.commands.filter(x => x.commandType == category);
        return (commandMemes.length/this.commands.length)*100;
    }
}