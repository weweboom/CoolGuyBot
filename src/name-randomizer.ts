import { Client, Message, Guild } from 'discord.js';
import { FileStreamer } from './file-streamer';
import { Server } from './models/server';
import * as request from 'request';
const randomWord = require('random-word');
export class NameRandomizer {
    private client: Client;
    private fileStreamer = new FileStreamer();
    private servers: Server[];
    private INTERVAL: number = 100000;
    private REGISTERED_USERS_PATH = './src/media/registered-users.json';
    private wordApiUrl = 'http://randomword.setgetgo.com/get.php';

    constructor(client: Client) {
        this.client = client;
        this.fileStreamer.read(this.REGISTERED_USERS_PATH)
            .then((data: Server[]) => {
                this.servers = data;
                this.nameLoop();
            });
    }

    private nameLoop() {
        setTimeout(() => {
            this.randomizeNames();
            this.nameLoop();
        }, this.INTERVAL);
    }

    private randomizeNames() {
        for (var server of this.servers) {
            var clientServer = this.client.guilds.get(server.id);
            console.log(`Getting users for server ${clientServer.name}`);
            if (clientServer) {
                for (var user of server.members) {
                    var clientUser = clientServer.members.get(user);
                    if (clientUser != undefined) {
                        console.log(`Setting nickname for user ${clientUser.id}`);                        
                        clientUser.setNickname(randomWord());
                    }
                }
            }
        }
    }

    register(message: Message) {
        let server = this.servers.filter(x => x.id == message.guild.id)[0]
        if(server.members.filter(x => x == message.member.id).length > 0) message.reply(`you're already registered!`);
        else server.members.push(message.member.id);
        this.fileStreamer.write(this.REGISTERED_USERS_PATH, this.servers)
        .catch((err: any) => {
            console.log(err);
        });
    }

    deregister(message: Message) {
        let server = this.servers.filter(x => x.id == message.guild.id)[0];
        if(server.members.filter(x => x == message.member.id).length != 1) message.reply(`you're not currently registered!`);
        let newMembers: string[] = new Array();
        for(var id of server.members) {
            if(id != message.author.id) newMembers.push(id);
        }
        server.members = newMembers;
        this.fileStreamer.write(this.REGISTERED_USERS_PATH, this.servers);
        message.member.setNickname(message.author.username);      
    }
}