export interface Command {
    commandType: string;
    command: string;
    path?: string;
    volume?: number;
}