import { Client, Message, Emoji } from "discord.js";
import { MessageRouter } from './message-router';
import { CharacterAlternatives } from './characterAlternatives'
import { NameRandomizer } from './name-randomizer'
var client = new Client();
const token = "";
const activationCharacter = "."
//const dwarvenEmoji = client.guilds.get('353635640782618635').emojis.get('353640927937560576');
let messageRouter: MessageRouter;

client.on('ready', () => {
    console.log('ready');
    messageRouter = new MessageRouter(activationCharacter, client);
    let nameRandomizer = new NameRandomizer(client);
    client.user.setGame(`thicc`);
});
client.on('message', message => {
    var altChar = new CharacterAlternatives(message.content);
    messageRouter.parse(message);
    if (altChar.isStringIllegal('secretdiscord')) {
        if (message.deletable) {
            message.delete();
        }
    }
});


client.on('presenceUpdate', (oldMember, newMember) => {
    let date = new Date();
    console.log(`User: ${newMember.displayName} Action: ${newMember.voiceChannel ? newMember.voiceChannel.name : 'No Voice Channel'} Time: ${`${date.getHours()}:${date.getMinutes()}`}}`);
});


client.login(token);