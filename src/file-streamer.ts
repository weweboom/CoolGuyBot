import * as fs from 'fs';

export class FileStreamer {

    read(path: string): any {
        return new Promise((resolve, reject) => {
            fs.readFile(path, 'utf8', (err, data) => {
                if(err) return reject(err);
                resolve(JSON.parse(data));
            });
        });
    }

    write(path: string, data: any): any {
        return new Promise((resolve, reject) => {
            fs.writeFile(path, JSON.stringify(data), (err) => {
                if (err) reject(err);
                resolve();
            });
        });
    }
}