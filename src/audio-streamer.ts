import { Message, User } from 'discord.js';
import { Command } from './models/command';
import * as fs from 'fs';
export class AudioStreamer {
    private isSpeaking: boolean;

    play(command: Command, message: Message) {
        if (message.member.voiceChannel) {
            message.member.voiceChannel.join()
                .then(connection => {
                    let channel = message.member.voiceChannel;
                    if (!this.isSpeaking) {
                        this.isSpeaking = true;
                        var play: any;
                        if (command.commandType == "local" || command.commandType == "alex" || command.commandType =="sunny") play = connection.playFile(command.path || "");
                        if (command.commandType == "web") play = connection.playArbitraryInput(command.path || "");
                        let dispatcher = play;
                        if(command.volume) {
                            dispatcher.setVolume(command.volume);
                        }
                        dispatcher.on('end', () => {
                            channel.leave();
                            this.isSpeaking = false;
                        })
                    }
                    else {
                        message.reply("you tryna spam bro? that's not chill");
                    }
                });
        }
        else {
            message.reply("you gotta be in a voice channel for that bruh");
        }
    }

    record(command: string, message: Message) {
        let channel = message.member.voiceChannel;
        if(message.member.voiceChannel) {
            message.member.voiceChannel.join()
            .then(connection => {
                let recorder = connection.createReceiver();                            
                connection.on('speaking', (user, speaking) => {
                    if(speaking) {
                        let audioStream = recorder.createPCMStream(user);
                        let fileName = `./src/media/audio/${command}.pcm`;
                        let outputStream = fs.createWriteStream(fileName);
                        audioStream.pipe(outputStream);
                        outputStream.on('error', function(e) { console.error(e); });
                        audioStream.on('data', (data: any) => {
                            console.log(data);
                        });
                        audioStream.on('end', () => {
                            outputStream.end();
                            channel.leave();
                        });
                    }
                });
            })
            .catch((err: any) => {
                console.log(err);
            });
        }
    }
}