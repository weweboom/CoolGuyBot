export class CharacterAlternatives {
    private str: string;
    private characterIndex: number;
    constructor(str: string) {
        this.str = str;
    }

    public isStringIllegal(illegalPattern: string) {
        this.characterIndex = 0;
        var currentCharacter = illegalPattern[this.characterIndex];
        for (var i = 0; i < this.str.length; i++) {
            if (this.routeToletter(currentCharacter, i)) {
                console.log(currentCharacter);
                if (this.characterIndex == illegalPattern.length - 1) return true //illegal string
                this.characterIndex++;
                currentCharacter = illegalPattern[this.characterIndex];
            }
            else if ('- /\\@!#$=%^)(*&~.,&aeiouy'.indexOf(this.str[i]) < 0 && this.characterIndex != 0) {
                this.characterIndex = 0;
                currentCharacter = illegalPattern[this.characterIndex];
            }
        }
    }

    private routeToletter(letter: string, index: number): boolean {
        switch (letter) {
            case 's': return this.isS(index);
            case 'e': return this.isE(index);
            case 'c': return this.isC(index);
            case 'r': return this.isR(index);
            case 't': return this.isT(index);
            case 'd': return this.isD(index);
            case 'i': return this.isI(index);
            case 'o': return this.isO(index);
            default: return false;
        }
    }
    private isS(i: number) {
        var c = this.initCharacter(i);      
        return this.containsCharacter('s$5', c);
    }

    private isE(i: number) {
        var c = this.initCharacter(i);
        return this.containsCharacter('e3', c);
    }

    private isC(i: number) {
        var c = this.initCharacter(i);
        return this.containsCharacter('c�(<ks', c);
    }

    private isR(i: number) {
        var c = this.initCharacter(i);
        if (this.containsCharacter('r', c)) return true;
        if (c == '|') {
            if ('2Z?'.indexOf(this.str[i + 1]) >= 0) {
                this.characterIndex++;
                return true;
            }
        }
        return false;
    }

    private isT(i: number) {
        var c = this.initCharacter(i);
        if (this.containsCharacter('t+7', c)) return true;
        if (c == ']') {
            if ('['.indexOf(this.str[i + i]) >= 0) {
                this.characterIndex++;
                return true;
            }
        }
        return false;
    }

    private isD(i: number) {
        var c = this.initCharacter(i);
        if (this.containsCharacter('d', c)) return true;
        if (c == '|') {
            if (this.containsCharacter(')>', this.str[i + i])) {
                this.characterIndex++;
                return true;
            }
        }
        if (c == '<') {
            if (this.containsCharacter('|', this.str[i + i])) {
                this.characterIndex++;
                return true;
            }
        }
        return false;
    }

    private isI(i: number) {
        var c = this.initCharacter(i);
        if (this.containsCharacter('i1!l|', c)) return true;
    }

    private isO(i: number) {
        var c = this.initCharacter(i);
        if (this.containsCharacter('oO0', c)) return true;
        if (c == '{') {
            if ('}'.indexOf(this.str[i + 1]) >= 0) {
                this.characterIndex++;
                return true;
            }
        }
        return false;
    }

    private initCharacter(i: number) {
        return this.str[i].toLocaleLowerCase();
    }

    private containsCharacter(characterList: string, character: string) {
        return characterList.indexOf(character) >= 0
    }
}