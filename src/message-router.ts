import { Message, Client } from "discord.js";
import { Command } from './models/command';
import { AudioStreamer } from './audio-streamer';
import { CommandCollection } from './command-collection';
import { NameRandomizer } from './name-randomizer'; 
const { exec } = require('child_process');
export class MessageRouter {
    private client: Client;
    private activationCharacter: string;
    private audioStreamer = new AudioStreamer();
    private commandCollection = new CommandCollection();
    //private nameRandomizer: NameRandomizer;
    private awaitingNewCommand: boolean = false;
    private awaitingUser: any;
    constructor(activationCharacter: string, client: Client) {
        this.client = client;
        //this.nameRandomizer = new NameRandomizer(this.client);
        if(activationCharacter.length != 1) {
            throw("Activation Character must be exactly one character in length");
        }
        this.activationCharacter = activationCharacter;        
    }

    parse(message: Message) {
        if(message.content[0] !== this.activationCharacter) return;
        if(message.deletable) {
            message.delete();
        }
        let arg: string = message.content.slice(1);        
        try {
            let command = this.commandCollection.find(arg);
            console.log(command.command + ' ' + command.commandType);
            if(command.commandType == 'random') {
                if(command.command == 'random') this.audioStreamer.play(this.commandCollection.findRandom(), message);
                if(command.command == 'alex' || command.command == 'sunny') {
                    console.log('reached');
                    this.audioStreamer.play(this.commandCollection.findRandom(command.command), message);
                }
            }
            if(command.commandType == 'help') {
                //message.reply(`\`\`\`md\n# The current commanderdogs are as follows: #\n${this.commandCollection.getCommands()}\`\`\``);
                let helpMessage = {
                    "author": {
                      "name": this.client.user.username,
                      "icon_url": this.client.user.avatarURL
                    },
                    "title": "Current Commanderdogs",
                    "description": this.commandCollection.getCommands(),
                    "footer": {
                        "icon_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3mh70MUkzwJwlzm6ZIWsnRAeeSUZz7vJ5mibB9bvF3ct73M-K",
                        "text": `I am currently ${this.commandCollection.commandLevel('alex').toPrecision(2)}% Alex Jones memes, and ${this.commandCollection.commandLevel('sunny').toPrecision(2)}% Sunny memes. Them are rookie numbers. YOU GOTTA GET THOSE NUMBERS UP` 
                    },
                  };
                message.channel.send({ embed: helpMessage });
            }
            if(command.commandType == 'record') {
                this.record(message);
            }
            //if(command.commandType == 'register') {
            //    this.nameRandomizer.register(message);
            //}
            //if(command.commandType == 'deregister') {
            //    this.nameRandomizer.deregister(message);
            //}
            if(command.commandType == 'shell') {
                exec('TaskKill /IM node.exe /F && node src/app.js')
            }
            if(['local', 'web', 'alex', 'sunny'].indexOf(command.commandType) != -1) {
                this.audioStreamer.play(command, message);          
            }
        }
        catch(ex) {
            console.log(ex);
            if(!this.awaitingNewCommand || message.author != this.awaitingUser) {
                message.reply(`${message.content} isn't one of my commands you autistic illiterate drooling waste of ball batter looking fuck`)                
            }
        }
    }

    private record(message: Message) {
        message.reply('What are you wanting the command to be? (leave off the activation character)');
        this.awaitingNewCommand = true;
        this.awaitingUser = message.author;
        message.client.on('message', newMessage => {
            if(this.awaitingNewCommand && message.author == newMessage.author) {
                if(newMessage.content[0] == this.activationCharacter) {
                    newMessage.reply('I said leave out the activation character... :thinking: :thinking: :thinking:')
                }
                else {
                    this.awaitingNewCommand = false;   
                    this.awaitingUser = undefined;                         
                    this.audioStreamer.record(newMessage.content, message);
                    this.commandCollection.addCommand({
                        command: newMessage.content,
                        commandType: 'local',
                        path: `./src/media/audio/${newMessage.content}.pcm`
                    });
                    this.commandCollection.updateCommands();
                }
            }
        })
    }
}